#include <algorithm>
#include <iostream>
#include <istream>
#include <iterator>
#include <ostream>
#include <string>
#include <type_traits>

using std::cin;
using std::cout;
using std::endl;
using std::string;

class Vehicle {
public:
  string numberSign;
  string color;
  string source;
  string manufacturer;
  float priceWithVAT;
  float priceWithoutVAT;
  void input() {
    cout << "\n---------------------\n";
    Vehicle::input();
    cout << "enter manufacturer: ";
    cin >> manufacturer;
    cout << "enter priceWithVAT: ";
    cin >> priceWithVAT;
    cout << "enter priceWithoutVAT: ";
    cin >> priceWithoutVAT;
  }
  float cash() {
    if (priceWithoutVAT <= 2000) {
      priceWithVAT = priceWithoutVAT + priceWithoutVAT * 10 / 100;
    } else {
      priceWithVAT = priceWithoutVAT + priceWithoutVAT * 15 / 100;
    }
    return priceWithVAT;
  }
};
std::istream &operator>>(std::istream &CIN, Vehicle &car) {
  cout << "numberSign: ";
  CIN >> car.numberSign;
  cout << "color: ";
  CIN >> car.color;
  cout << "source: ";
  CIN >> car.source;
  cout << "enter manufacturer: ";
  CIN >> car.manufacturer;
  cout << "enter priceWithVAT: ";
  CIN >> car.priceWithVAT;
  cout << "enter priceWithoutVAT: ";
  CIN >> car.priceWithoutVAT;

  return CIN;
}
std::ostream &operator<<(std::ostream &COUT, Vehicle &car) {
  cout << "numberSign: ";
  COUT << car.numberSign << endl;
  cout << "color: ";
  COUT << car.color << endl;
  cout << "source: ";
  COUT << car.source << endl;
  cout << "enter manufacturer: ";
  COUT << car.manufacturer;
  cout << "enter priceWithVAT: ";
  COUT << car.priceWithVAT;
  cout << "enter priceWithoutVAT: ";
  COUT << car.priceWithoutVAT;

  return COUT;
}
class CarList {
  int n = 3;

public:
  Vehicle *car = new Vehicle[n];
  void intputCar() {
    for (int i = 0; i < n; i++) {
      cin >> car[i];
    }
  }
  void outputCar() {
    cout << "\n----------------------\n";
    for (int i = 0; i < n; i++) {
      cout << car[i];
      cout << "\n------------------------\n";
    }
  }
  void sort() {
    int min;
    for (int i = 0; i < n; i++) {
      min = i;
      for (int j = i + 1; j < n; j++) {
        if (car[min].priceWithoutVAT > car[j].priceWithoutVAT) {
          min = j;
        }
      }
      if (min != i) {
        std::swap(car[i], car[min]);
      }
    }
  }
  int sumOfCash() {
    int sum{};
    for (int i = 0; i < n; i++) {
      sum += car[i].priceWithVAT;
    }
    return sum;
  }
  void maxCash() {
    int max = car[0].priceWithoutVAT;
    int count_max = 0;
    for (int i = 1; i < n; i++) {
      if (car[i].priceWithoutVAT > max)
        max = car[i].priceWithoutVAT;
      count_max = i;
    }
    cout << "\n" << car[count_max];
  }
};

int main(int argc, char *argv[]) {
  Vehicle *car1 = new Vehicle;
  car1->input();
  car1->cash();
  CarList carlist;
  cout << "\nA.#input#\n";
  carlist.intputCar();
  cout << "\nB.#output#\n";
  carlist.outputCar();
  carlist.sort();
  cout << "\nC.#sau khi sap xep#";
  carlist.outputCar();
  cout << "sum of cash: " << endl << carlist.sumOfCash();
  cout << "\nD.#maxCash: ";
  carlist.maxCash();
  return 0;
}
