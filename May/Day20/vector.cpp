#include <cmath>
#include <iostream>
#include <math.h>
#include <vector>

using std::cin;
using std::cout;
// using std::endl;
using std::vector;

class Array {
  int n = 4;

public:
  vector<int> a;
  void inputVector() {
    a.resize(n);
    for (int i = 0; i < n; i++) {
      cin >> a[i];
    }
  }
  void displayVector() {
    std::cout << "Vector elements: ";
    for (auto &count : a) {
      std::cout << count << " ";
    }
    std::cout << std::endl;
  }
  int sumVector() {
    int sum = 0;
    for (int i = 0; i < a.size(); i++) {
      sum += a[i];
    }
    return sum;
  }
  bool checkSnt(int n) { // kiểm tra số nguyên tố
    if (n < 2)
      return false;
    for (int i = 2; i <= sqrt(n); i++) {
      if (n % i == 0)
        return false;
    }
    return true;
  }
  void soNguyento() {
    for (int i = 0; i < a.size(); i++) {
      if (checkSnt(a[i]) == true) {
        cout << a[i] << " ";
      }
    }
  }
  bool checkScp(int n) { // kiểm tra số chính phương
    if (sqrt(n) == (int)sqrt(n))
      return true;
    return false;
  }
  void soChinhphuong() {
    for (int i = 0; i < a.size(); i++) {
      if (checkScp(a[i]) == true)
        cout << a[i] << " ";
    }
  }
};

int main(int argc, char *argv[]) {
  Array *obj = new Array;
  obj->inputVector();
  obj->displayVector();
  cout << "sum in vector: ";
  cout << obj->sumVector();
  cout << "\nso nguyen to in vector: ";
  obj->soNguyento();
  cout << "\nso chinh phuong in vector: ";
  obj->soChinhphuong();
  delete obj;
  return 0;
}
