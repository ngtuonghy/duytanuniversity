#include <iostream>
#include <iterator>
#include <string>
#include <utility>
#include <vector>
using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;

class employee {
protected:
  int id;
  string name;
  float salary;
  int year;

public:
  std::vector<employee> listPatient;
  void input() {
    cout << "nhap id: ";
    cin >> id;
    cin.ignore();
    cout << "nhap name: ";
    std::getline(cin, name);
    cout << "nhao year: ";
    cin >> year;
    cout << "nhap salary: ";
    cin >> salary;
  }
  void output() const {
    cout << "ID: " << id << endl;
    cout << "name: " << name << endl;
    cout << "year: " << year << endl;
    cout << "salary: " << salary << endl;
    cout << "-----------------------\n";
  }
  void inputEmployee(int n) {
    employee emp;
    for (int i = 0; i < n; i++) {
      emp.input();
      listPatient.push_back(emp);
    }
  }
  void display() {
    std::cout << std::endl;
    std::cout << "Employee details:" << std::endl;
    for (const auto employee : listPatient) {
      employee.output();
      std::cout << std::endl;
    }
  }
};

int main(int argc, char *argv[]) {
  employee t;
  int n;
  cout << "nhap so luong phan tu mang: ";
  cin >> n;

  t.inputEmployee(n);
  t.display();
  return 0;
}
