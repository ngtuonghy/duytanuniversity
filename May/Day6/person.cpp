#include "person.h"
#include <iostream>
using std::cin;
using std::cout;
using std::endl;

void person::input() {
  cout << "id: ";
  cin >> id;
  cout << "name: ";
  cin >> name;
}
void person::output() {
  cout << "id: " << id << endl;
  cout << "name: " << name << endl;
}
