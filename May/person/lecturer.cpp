#include "lecturer.h"
#include "person.h"

void Lecturer::input() {
  Person::input();
  cout << "empID: ";
  cin >> empID;
  cout << "numberofPaper: ";
  cin >> numberOfPaper;
}
void Lecturer::output() {
  Person::output();
  cout << "empID: " << empID << endl;
  cout << "numberofPaper: " << numberOfPaper << endl;
}
double Lecturer::calculateBonuns() {
  if (numberOfPaper > 3) {
    return 1500000;
  } else {
    return 0;
  }
}
