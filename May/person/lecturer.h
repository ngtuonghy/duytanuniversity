#ifndef LECTURER_H
#define LECTURER_H

#include "person.h"

class Lecturer : public Person {
public:
  int empID;
  int numberOfPaper;
  void input();
  void output();
  double calculateBonuns();
};
#endif // DEBUG
