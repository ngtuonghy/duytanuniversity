#include "student.h"

void Student::input() {
  Person::input();
  cout << "studID: ";
  cin >> studID;
  cout << "avgMark: ";
  cin >> avgMark;
}
void Student::output() {
  Person::output();
  cout << "studID: " << studID << endl;
  cout << "avgMark: " << avgMark << endl;
}
double Student::calculateBonuns() {
  if (avgMark > 18) {
    return 750000;
  } else {
    return 0;
  }
}
