#include <iostream>

template <class T> T _max(T a, T b) {
  if (a > b)
    return a;
  return b;
}
template <class U> U _Max(U a, U b, U c) {
  if (a > _max(b, c))
    return a;
  return _max(b, c);
}
template <class A> A findMaxArr(A arr[], int n) {
  A Max = arr[0];
  for (int i = 0; i < n; i++) {
    if (arr[i] > Max)
      Max = arr[i];
  }
  return Max;
}
int main(int argc, char *argv[]) {
  std::cout << "Max in three number: " << _Max(10, 20, 30);
  float arr[5]{1.2, 1.5, 1.8, 8.1, 5.5};
  float arrInt[5]{2, 5, 8, 9, 20};
  std::cout << "\nMax in array float: " << findMaxArr(arr, 5);
  std::cout << "\nMax in array int : " << findMaxArr(arrInt, 5);
  return 0;
}
