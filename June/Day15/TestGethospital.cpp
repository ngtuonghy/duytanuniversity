#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>
using namespace std;
class Person {
  string fullName;
  int yearBorn;

public:
  int getYearborn() { return yearBorn; }
  string getFullname() { return fullName; }
  virtual void input() {
    cout << "Enter full name: ";
    cin.ignore();
    getline(cin, this->fullName);
    cout << "Enter year Born: ";
    cin >> this->yearBorn;
  }
  virtual void output() {
    cout << "full name: ";
    cout << this->fullName;
    cout << endl;
    cout << "year Born: ";
    cout << this->yearBorn;
    cout << endl;
  }
  virtual int Charge() = 0;
};
class Outpatient : public Person {
public:
  string nameSick;
  int costsOftreatment;
  void input() {
    Person::input();
    cout << "enter Name Sick: ";
    cin.ignore();
    getline(cin, this->nameSick);
    cout << "enter costs of treatment: ";
    cin >> costsOftreatment;
  }
  void output() {
    cout << "-----------------------------\n";
    cout << endl;
    Person::output();
    cout << "Name Sick: ";
    cout << this->nameSick;
    cout << endl;
    cout << "costs of treatment: ";
    cout << this->costsOftreatment;
    cout << endl;
  }
  int Charge() {
    float percent = 8.0 / costsOftreatment;
    int sumCosts;
    if (this->getYearborn() < 1960) {
      int sumPercent = costsOftreatment * percent;
      int sumCosts = costsOftreatment - sumPercent;
      return sumCosts;
    }
    return costsOftreatment;
  }
};
class Inpatient : public Person {
public:
  string diagnosticFirst;
  int costsOfmedicine;
  int costsOfstay;
  void input() {
    Person::input();
    cout << "enter diagnostic first: ";
    cin.ignore();
    getline(cin, this->diagnosticFirst);
    cout << "enter costs of medicine: ";
    cin >> this->costsOfmedicine;
    cout << "enter costs of stay: ";
    cin >> this->costsOfstay;
  }
  void output() {
    cout << "-----------------------------\n";
    cout << endl;
    Person::output();
    cout << "diagnostic first: ";
    cout << this->diagnosticFirst;
    cout << endl;
    cout << "costs of medicine: ";
    cout << this->costsOfmedicine;
    cout << endl;
    cout << "costs of stay: ";
    cout << this->costsOfstay;
    cout << endl;
  }
  int Charge() {
    float percentMedicine = 10.0 / 100.0;
    float percentStay = 12.0 / 100.0;
    int sumCosts = 0;
    if (this->getYearborn() < 1960) {
      int sumPercentMedicine = costsOfmedicine * percentMedicine;
      int sumPercentStay = costsOfstay * percentStay;
      sumCosts = (costsOfmedicine - sumPercentMedicine) +
                 (costsOfstay - sumPercentStay);
      return sumCosts;
    }
    return costsOfmedicine + costsOfstay;
  }
};
bool Xgreater(Person *ps1, Person *ps2) {
  return ps1->getFullname() < ps2->getFullname();
}
class Dispensary {
public:
  int n = 10;
  // int countArr = 0;
  int locationArr;
  Person **arr = new Person *[n];
  void copy() {}
  void inputDispensary() {
    locationArr = 0;
    int x;
    while (true) {
      cout << "(1/2/0) 1-Outpatient 2-Inpatient 0-Exit: ";
      cin >> x;

      if (x == 1) {
        arr[locationArr] = new Outpatient;
      }
      if (x == 2) {
        arr[locationArr] = new Inpatient;
      }
      if (x == 0) {
        break;
      }
      arr[locationArr]->input();
      locationArr++;
      n++;
    }
  }
  void outputDispensary() {
    cout << "count: " << locationArr;
    cout << "n co gia tri: " << n;
    for (int i = 0; i < locationArr; i++) {
      arr[i]->output();
    }
  }
  void sumCharge() {
    int sum = 0;
    for (int i = 0; i < locationArr; i++) {
      sum += arr[i]->Charge();
    }
    cout << "Sum: " << sum << endl;
  }
  void sortDispensary() { sort(arr, arr + locationArr, Xgreater); }
  ~Dispensary() { delete[] arr; }
};

int main(int argc, char *argv[]) {
  Dispensary di;

  di.inputDispensary();
  system("clear");
  di.outputDispensary();
  di.sumCharge();
  di.sortDispensary();
  cout << endl;
  cout << "========= sau sap xep ==========\n";
  di.outputDispensary();
  return 0;
}
