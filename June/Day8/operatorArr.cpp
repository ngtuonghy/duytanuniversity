#include <algorithm>
#include <functional>
#include <iomanip>
#include <ios>
#include <iostream>
#include <string>
#include <vector>
using namespace std;
class Employee {
public:
  int id;
  std::string name;
  int salary;
  friend istream &operator>>(std::istream &CIN, Employee &obj) {
    std::cout << "nhap id: ";
    CIN >> obj.id;
    cout << "nhap ten: ";
    cin.ignore();
    getline(CIN, obj.name);
    cout << "nhap salary: ";
    CIN >> obj.salary;
    return CIN;
  }
  void table() {
    std::cout << left << setw(20) << "id" << setw(20) << "name" << setw(20)
              << "salary" << endl;
  }
  friend ostream &operator<<(ostream &COUT, const Employee &obj) {
    COUT << left << setw(20) << obj.id << setw(20) << obj.name << setw(20)
         << obj.salary << endl;
    return COUT;
  }
  void inputArremplyee(vector<Employee> &arr, int n) {
    for (int i = 0; i < n; i++) {
      Employee human;
      cin >> human;
      arr.push_back(human);
    }
  }
  void outputArremployee(vector<Employee> &arr) {
    table();
    for (int i = 0; i < arr.size(); i++) {
      cout << arr[i];
    }
  }
  //  bool operator<(const Employee &obj) { return salary < obj.salary; }
};
template <class T> struct Zagreater {
  bool operator()(const T &obj1, const T &obj2) {
    return obj1.salary > obj2.salary;
  }
};
template <class T> struct Azgreater {
  bool operator()(const T &obj1, const T &obj2) {
    return obj1.salary < obj2.salary;
  }
};

int main(int argc, char *argv[]) {
  Employee human1;
  vector<Employee> arr;
  void sortArrAz();
  int n;
  cout << "nhap so luong mang: ";
  cin >> n;
  human1.inputArremplyee(arr, n);
  cout << "ban vua nhap\n";
  human1.outputArremployee(arr);
  cout << "tu be den lon: \n";
  sort(arr.begin(), arr.end(), Azgreater<Employee>());
  human1.outputArremployee(arr);
  cout << "tu lon den be: \n";
  sort(arr.begin(), arr.end(), Zagreater<Employee>());
  human1.outputArremployee(arr);
  return 0;
}
