#include <iostream>
#include <istream>
#include <numeric>
#include <string>

using namespace std;

struct Praction {
  int tu;
  int mau;
  Praction(int tu = 0, int mau = 1) {
    this->tu = tu;
    this->mau = mau;
  };
  int gcdEuclid(int a, int b) {
    if (b == 0) {
      return a;
    }
    return gcdEuclid(b, a % b);
  }
  Praction recude() { // rút gọn phân số
    int divisor = gcdEuclid(this->tu, this->mau);
    int newTu = this->tu / divisor;
    int newMau = this->mau / divisor;
    return Praction(newTu, newMau);
  }
  Praction operator+(const Praction &other) {
    int newTu = this->tu * other.mau + other.tu * this->mau;
    int newMau = this->mau * other.mau;
    return Praction(newTu, newMau).recude();
  }
  Praction operator-(const Praction &other) {
    int newTu = this->tu * other.mau - other.tu * this->mau;
    int newMau = this->mau * other.mau;
    return Praction(newTu, newMau).recude();
  }
  Praction operator*(const Praction &other) {
    int newTu = this->tu * other.tu;
    int newMau = this->mau * other.mau;
    return Praction(newTu, newMau).recude();
  }
  Praction operator/(const Praction &other) {
    int newTu = this->tu * other.mau;
    int newMau = this->mau * other.tu;
    return Praction(newTu, newMau).recude();
  }
  friend istream &operator>>(istream &CIN, Praction &obj) {
    cout << "nhap tu: ";
    CIN >> obj.tu;
    cout << "nhap mau: ";
    CIN >> obj.mau;
    return CIN;
  }
  friend ostream &operator<<(ostream &COUT, const Praction &obj) {
    COUT << obj.tu << "/" << obj.mau;
    return COUT;
  }
};

class Classprac {
public:
  int tu;
  int mau;
  Classprac(int tu = 0, int mau = 1) {
    this->tu = tu;
    this->mau = mau;
  };
  int gcdEuclid(int a, int b) {
    if (b == 0) {
      return a;
    }
    return gcdEuclid(b, a % b);
  }
  Classprac recude() { // rút gọn phân số
    int divisor = gcdEuclid(this->tu, this->mau);
    int newTu = this->tu / divisor;
    int newMau = this->mau / divisor;
    return Classprac(newTu, newMau);
  }
  Classprac operator+(const Classprac &other) {
    int newTu = this->tu * other.mau + other.tu * this->mau;
    int newMau = this->mau * other.mau;
    return Classprac(newTu, newMau).recude();
  }
  Classprac operator-(const Classprac &other) {
    int newTu = this->tu * other.mau - other.tu * this->mau;
    int newMau = this->mau * other.mau;
    return Classprac(newTu, newMau).recude();
  }
  Classprac operator*(const Classprac &other) {
    int newTu = this->tu * other.tu;
    int newMau = this->mau * other.mau;
    return Classprac(newTu, newMau).recude();
  }
  Classprac operator/(const Classprac &other) {
    int newTu = this->tu * other.mau;
    int newMau = this->mau * other.tu;
    return Classprac(newTu, newMau).recude();
  }
  friend istream &operator>>(istream &CIN, Classprac &obj) {
    cout << "nhap tu: ";
    CIN >> obj.tu;
    cout << "nhap mau: ";
    CIN >> obj.mau;
    return CIN;
  }
  friend ostream &operator<<(ostream &COUT, const Classprac &obj) {
    COUT << obj.tu << "/" << obj.mau;
    return COUT;
  }
};
class Sophuc {
public:
  int thuc;
  int ao;
  Sophuc(int t, int a) : thuc(t), ao(a) {}
  Sophuc operator+(const Sophuc &other) {
    int newThuc = this->thuc + other.thuc;
    int newAo = this->ao + other.ao;
    return Sophuc(newThuc, newAo);
  }
  friend istream &operator>>(istream &CIN, Sophuc &obj) {
    cout << "nhap thuc: ";
    CIN >> obj.thuc;
    cout << "nhap ao: ";
    CIN >> obj.ao;
    return CIN;
  }
  friend ostream &operator<<(ostream &COUT, const Sophuc &obj) {
    COUT << obj.thuc << "+" << obj.ao << "i";
    return COUT;
  }
};
int main(int argc, char *argv[]) {
  Praction ps1(1, 2);
  Praction ps2(3, 4);
  Praction sum = ps1 + ps2;
  cout << endl;
  cout << "phep cong: ";
  cout << sum;
  Praction sum1 = ps1 - ps2;
  cout << endl;
  cout << "phep tru: ";
  cout << sum1;
  Praction sum2 = ps1 * ps2;
  cout << endl;
  cout << "phep nhan: ";
  cout << sum2;
  Praction sum3 = ps1 / ps2;
  cout << endl;
  cout << "phep chia: ";
  cout << sum3;
  cout << endl;
  cout << "=====================";
  cout << "\nthis is class: \n";
  Classprac cps1(4, 5);
  Classprac cps2(6, 7);
  Classprac csum = cps1 + cps2;
  cout << "phep cong: ";
  cout << csum;
  Classprac csum1 = cps1 - cps2;
  cout << endl;
  cout << "phep tru: ";
  cout << csum1;
  Classprac csum2 = cps1 * cps2;
  cout << endl;
  cout << "phep nhan: ";
  cout << csum2;
  Classprac csum3 = cps1 / cps2;
  cout << endl;
  cout << "phep chia: ";
  cout << csum3;
  // so phuc;
  cout << "\n==================\n";
  Sophuc sp1(1, 2);
  Sophuc sp2(3, 4);
  Sophuc spSum = sp1 + sp2;
  cout << "so phuc: " << spSum;
  return 0;
}
