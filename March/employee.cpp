#include <iomanip>
#include <iostream>
#include <iterator>
#include <string>
#include <utility>

using std::cin;
using std::cout;
using std::endl;
using std::string;

typedef struct {
  int id;
  string name;
  int telephone;
  string address;
  double salary;
} Employee;

void inemployee(Employee *employee, int n);
void outemployee(Employee *employee, int n);
void sortEmployee(Employee employee[], int n);
void find(Employee employee[], int n);
void deleteArr(Employee employee[], int location, int &n);
void insertArr(Employee employee[], int &n, int location);
int sumArr(Employee employee[], int n);

int main(int argc, char *argv[]) {
  int n = 2;
  Employee *employee = new Employee[n];
  inemployee(employee, n);
  outemployee(employee, n);
  sortEmployee(employee, n);
  cout << "-----------sau sap xep-----------\n";
  outemployee(employee, n);
  find(employee, n);
  cout << endl;
  int k;
  cout << "\n---------------------------\nhap vi tri can xoa: ";
  cin >> k;
  deleteArr(employee, k, n);
  outemployee(employee, n);
  cout << "-------------------";
  int m;
  cout << "\nnhap vi tri chen: ";
  cin >> m;
  insertArr(employee, n, m);
  outemployee(employee, n);
  cout << "sum of arr" << sumArr(employee, n);
  delete[] employee;
  cout << "delete" << n;
  return 0;
}
void inemployee(Employee *employee, int n) {
  for (int i = 0; i < n; i++) {
    cout << "employee " << i + 1 << endl;
    cout << "id: ";
    cin >> employee[i].id;
    cout << "name: ";
    cin.ignore(32767, '\n');
    std::getline(cin, employee[i].name);
    cout << "telephone: ";
    cin >> employee[i].telephone;
    cout << "address: ";
    cin >> employee[i].address;
    cout << "salary: ";
    cin >> employee[i].salary;
  }
}
void outemployee(Employee *employee, int n) {
  cout << "\nstt\tid\tname\t\t\ttelephone\taddress\t\tsalary";
  for (int i = 0; i < n; i++) {
    cout << "\n" << i + 1;
    cout << "\t" << employee[i].id;
    cout << "\t" << employee[i].name;
    cout << "\t\t" << employee[i].telephone;
    cout << "\t\t" << employee[i].address;
    cout << "\t" << employee[i].salary;
    cout << "\n";
  }
}
void sortEmployee(Employee employee[], int n) {
  int min;
  for (int i = 0; i < n; i++) {
    min = i;
    for (int j = i + 1; j < n; j++) {
      if (employee[j].salary < employee[min].salary) {
        min = j;
      }
      std::swap(employee[i], employee[min]);
    }
  }
}
void find(Employee employee[], int n) {
  cout << "--salaty high--\n";
  int termHigh = n - 1;
  int termLow = 0;
  cout << "id: " << employee[termHigh].id
       << " name: " << employee[termHigh].name
       << " telephone: " << employee[termHigh].telephone
       << " salary: " << employee[termHigh].salary;
  cout << "\n--salary low--\n";
  cout << "id: " << employee[termLow].id << " name: " << employee[termLow].name
       << " telephone: " << employee[termLow].telephone
       << " salary: " << employee[termLow].salary;
}
void deleteArr(Employee employee[], int location, int &n) {
  for (int i = location; i < n - 1; i++) {
    employee[i] = employee[i + 1];
  }
  n--;
}
void insertArr(Employee employee[], int &n, int location) {
  int i = n;
  for (; i >= location; i--) {
    employee[i] = employee[i - 1];
  }
  n++;
  cout << "id: ";
  cin >> employee[location].id;
  cout << "name: ";
  cin.ignore(32767, '\n');
  std::getline(cin, employee[location].name);
  cout << "telephone: ";
  cin >> employee[location].telephone;
  cout << "salary: ";
  cin >> employee[location].salary;
}
int sumArr(Employee employee[], int n) {
  int sum{};
  for (int i = 0; i < n; i++) {
    sum = sum + employee[i].salary;
  }
  return sum;
}
