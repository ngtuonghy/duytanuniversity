#include <cmath>
#include <cstdlib>
#include <iostream>

using namespace std;

void simpleEquation();
void quadraticEquations();
void menu();
void xuliMenu();
int selectMenu();

int main() {
  menu();
  while (true) {
    xuliMenu();
  }
  return 0;
}
void menu() {
  cout << "======================================\n";
  cout << "moi ban chon so:\n1.simpleEquation\n2.quadraticEquations\n0.Quit\n";
  cout << "======================================\n";
}
int selectMenu() {
  int n;
  cout << "moi chon menu ";
  cin >> n;
  if (n >= 0 || n < 3) {
    return n;
  } else {
    return selectMenu();
  }
}
void xuliMenu() {
  int select = selectMenu();
  switch (select) {
  case 1:
    simpleEquation();
    break;
  case 2:
    quadraticEquations();
    break;
  case 0:
    cout << "Quit\n";
    exit(1);
  }
}
void simpleEquation() {
  float a, b;
  cout << "Nhap vao ba he so a, b: ";
  cin >> a >> b;
  if (a == 0) {
    if (b == 0) {
      cout << "Phuong trinh vo so nghiem" << endl;
    } else {
      cout << "Phuong trinh vo nghiem" << endl;
    }
  } else {
    cout << "x = " << -b / a << endl;
  }
}
void quadraticEquations() {
  float a, b, c;
  cout << "Nhap vao ba he so a, b, c: ";
  cin >> a >> b >> c;
  float delta = b * b - (4 * a * c);
  if (delta == 0) {
    float x = -b / (2 * a);
    cout << "Phuong trinh co nghiem kep " << x << endl;
  } else if (delta > 0) {
    float x1 = (-b + sqrt(delta)) / (2 * a);
    float x2 = (-b - sqrt(delta)) / (2 * a);
    cout << "Phuong trinh co hai nghiem x1 = " << x1 << ", x2 = " << x2 << endl;
  } else {
    cout << "Phuong trinh vo nghiem" << endl;
  }
}
