#include <cmath>
#include <iostream>

using namespace std;

enum MyEnum {
  la_so_nguyen_to,
  khong_phai_nguyen_to,
  la_so_hoan_hao,
  khong_phai_so_hoan_hao,
  la_so_chinh_phuong,
  khong_phai_so_chinh_phuong,
};

void nhap_mang(int *a, int n);
void xuat_mang(int *a, int n);
bool check_snt(int a);
void hien_thi_so_nguyen_to(int *a, int n);
void xoa_so_nguyen_to(int *a, int &n);
void xoa_so(int *a, int &n, int vitri);
void xuat_mang(int *a, int n);
int check_so_hoan_hao(int n);
void so_hoan_hao(int *a, int n);
int check_so_chinh_phuong(int n);
void so_chinh_phuong(int *a, int n);

int main() {
  cout << "-------------NGUYENTUONGHY_0564-------------\n";
  int n;
  cout << "so phan tu can nhap: ";
  cin >> n;
  int *a = new int[n];
  nhap_mang(a, n);
  cout << "mang vua nhap la\n";
  xuat_mang(a, n);
  cout << "so nguyen to co trong mang la: ";
  hien_thi_so_nguyen_to(a, n);
  cout << "\nso chinh phuong trong mang la: ";
  so_chinh_phuong(a, n);
  cout << "\n-----------------------------------";
  cout << "\nmang sau khi xoa so nguyen to\n";
  xoa_so_nguyen_to(a, n);
  xuat_mang(a, n);
  delete[] a;
  return 0;
}
void nhap_mang(int *a, int n) {
  int i = 0;
  while (i < n) {
    cout << "a[" << i << "]= ";
    cin >> a[i];
    i++;
  }
}
void xuat_mang(int *a, int n) {
  for (int i = 0; i < n; i++) {
    cout << "a[" << i << "]= " << a[i] << endl;
  }
}
bool check_snt(int n) {
  if (n < 2) {
    return khong_phai_nguyen_to;
  }
  for (int i = 2; i <= sqrt(n); i++) {
    if (n % i == 0) {
      return khong_phai_nguyen_to;
    }
  }
  return la_so_nguyen_to;
}
void hien_thi_so_nguyen_to(int *a, int n) {
  int i = 0;
  while (i < n) {
    if (check_snt(a[i]) == la_so_nguyen_to)
      cout << a[i] << " ";
    i++;
  }
}
void xoa_so(int *a, int &n, int vitri) {

  for (int i = vitri; i < n - 1; i++) {
    a[i] = a[i + 1];
  }
  n--;
}
void xoa_so_nguyen_to(int *a, int &n) {
  for (int i = 0; i < n; i++) {
    if (check_snt(a[i]) == la_so_nguyen_to) {
      xoa_so(a, n, i);
      i--;
    }
  }
}

int check_so_hoan_hao(int n) {
  int sum = 0;
  int i = 1;
  for (; i < n; i++) {
    if (n % i == 0) {
      sum += i;
    }
  }
  if (sum == n)
    return la_so_hoan_hao;
  return khong_phai_so_hoan_hao;
}
void so_hoan_hao(int *a, int n) {
  int i = 0;
  while (i < n) {
    if (check_so_hoan_hao(a[i]) == la_so_hoan_hao)
      cout << a[i] << " ";
    i++;
  }
}
int check_so_chinh_phuong(int n) {
  if (sqrt(n) == (int)sqrt(n))
    return la_so_chinh_phuong;
  return khong_phai_so_chinh_phuong;
}
void so_chinh_phuong(int *a, int n) {
  for (int i = 0; i < n; i++) {

    if (check_so_chinh_phuong(a[i]) == la_so_chinh_phuong) {
      cout << a[i] << " ";
    }
  }
}
