#include <cstdio>
#include <functional>
#include <iostream>
#include <iterator>
#include <ostream>

using std::cin;
using std::cout;
int MAX = 2;
struct Fac {
  double numerator;   // tu so
  double denominator; // mau so
};
void fraction(Fac *fac);
int quydong(Fac *fac);
int addFac(Fac *fac);
int subfac(Fac *fac);
void multi(Fac *fac);
void divi(Fac *fac);

int main(int argc, char *argv[]) {
  Fac fac[MAX];
  fraction(fac);
  for (int i = 0; i < MAX; i++) {
    cout << fac[i].numerator << "/" << fac[i].denominator << std::endl;
  }
  cout << "-----------------------------------\n";
  cout << "addFac: ";
  cout << addFac(fac) << "/" << quydong(fac) << std::endl;
  cout << "subFac: " << subfac(fac) << "/" << quydong(fac) << std::endl;
  cout << "multi: ";
  multi(fac);
  cout << std::endl << "divi: ";
  divi(fac);
  return 0;
}

void fraction(Fac *fac) {
  for (int i = 0; i < MAX; i++) {
    cout << "phan so thu " << i << std::endl;
    cout << "tu so: ";
    cin >> fac[i].numerator;
    cout << "mau so: ";
    cin >> fac[i].denominator;
  }
}
int quydong(Fac *fac) {
  double temp = fac[0].denominator * fac[1].denominator;
  return temp;
}
int addFac(Fac *fac) {
  int temp = fac[0].numerator + fac[1].numerator;

  return temp;
}
int subfac(Fac *fac) {
  int temp = fac[0].numerator - fac[1].numerator;
  return temp;
}
void multi(Fac *fac) {
  int numer = fac[0].numerator * fac[1].numerator;
  int denom = fac[0].denominator * fac[1].denominator;
  cout << numer << "/" << denom;
}
void divi(Fac *fac) {
  int numer = fac[0].numerator * fac[1].denominator;
  int denom = fac[0].denominator * fac[1].numerator;
  cout << numer << "/" << denom;
}
